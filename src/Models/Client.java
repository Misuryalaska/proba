package Models;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Client {
    private int id;
    private String lastname,name,otchestvo;
    private int count_lots;
    private int count_bets;
    private Date date;
    SimpleDateFormat format1 = new SimpleDateFormat("dd.MM.yyyy");

    public Client(int id, String lastname, String name, String otchestvo,Date date) {
        this.id = id;
        this.lastname = lastname;
        this.name = name;
        this.otchestvo = otchestvo;
        this.date = date;
    }

    public Client(int id, String lastname, String name, String otchestvo) {
        this.id = id;
        this.lastname = lastname;
        this.name = name;
        this.otchestvo = otchestvo;
    }

    public Client(String lastname,String name) {
        this.lastname = lastname;
        this.name = name;
    }

    public int getCount_lots() {
        return count_lots;
    }

    public void setCount_lots(int count_lots) {
        this.count_lots = count_lots;
    }

    public int getCount_bets() {
        return count_bets;
    }

    public void setCount_bets(int count_bets) {
        this.count_bets = count_bets;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public Date getDate()  {
        /*String pattern = "yyyy-MM-dd";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
        String dating = simpleDateFormat.format(date);
        Date dateee = simpleDateFormat.parse(dating);*/
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getOtchestvo() {
        return otchestvo;
    }

    public void setOtchestvo(String otchestvo) {
        this.otchestvo = otchestvo;
    }

    @Override
    public String toString() {
        String pattern = "yyyy-MM-dd";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
        String date = simpleDateFormat.format(new Date());
        System.out.println(date);
        return "Name: "+getName()+"\nCount lots: "+getCount_lots()+"\nCount bets: "+getCount_bets();
    }
}
