package Interface;

import Models.Client;
import Models.Product;

import java.util.List;

public interface ClientsActions {
    public void addClient(Client client);
    public void addProduct(Product product);
    public List<Product> displayProducts();
    public List<Client> displayClient();
}
