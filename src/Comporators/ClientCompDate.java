package Comporators;

import Models.Client;

import java.text.SimpleDateFormat;
import java.util.Comparator;

public class ClientCompDate implements Comparator<Client> {

    @Override
    public int compare(Client o1, Client o2) {
        if (o1.getDate().after(o2.getDate()))
            return -1;
        else if (o1.getDate().before(o2.getDate()))
            return 1;
        else
            return 0;
    }
}
