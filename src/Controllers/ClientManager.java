package Controllers;

import Common.Common;
import Comporators.ClientCompDate;
import Comporators.ClientCompLastName;
import Comporators.ClientCompName;
import Comporators.ClientCompOtchestvo;
import Interface.ClientsActions;
import Models.Client;
import Models.Product;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.*;

public class ClientManager implements ClientsActions {
    private static List<Client> clientList = new ArrayList<>();
    private static ObservableList<Product> productList = FXCollections.observableArrayList();
    Common common = new Common();

    private static Client client;

    public static void add(Product product)
    {
        productList.add(product);
    }

    public static Client writeUser(int id, String lastname, String name, String otchestvo, Date date)
    {
        client = new Client(id,lastname,name,otchestvo,date);
        clientList.add(client);
        return client;
    }

    public static void add(Client client)
    {
        clientList.add(client);
    }

    public static String getNameClient()
    {
         return client.getName();
    }
    public static String getLastNameClient()
    {
        return client.getLastname();
    }
    public static String getOtchestvo()
    {
        return client.getOtchestvo();
    }

    @Override
    public void addClient(Client client) {
        clientList.add(client);
    }

    @Override
    public void addProduct(Product product) {
        productList.add(product);
    }

    @Override
    public ObservableList<Product> displayProducts() {
        return productList;
    }

    public static void getLotsWithCategory(String category)
    {
            for (Product product : productList) {
                if (product.getCategory().equals(category)) {
                    System.out.println(product.getName_product());
                }
            }
    }

    public static void getBetsWithLots(String lot)
    {
        int number =1;
        for (Product product : productList) {
            if (product.getName_product().equals(lot)) {
                for (Integer bet : product.getBet())
                {
                    System.out.println("Ставка №"+number+" "+bet);
                    number++;
                }
            }
        }
    }

    public static void getYourLotsAndBets(int id)
    {
        int number = 1;
        int number_product = 1;
        for (Product product : productList)
        {
            if(product.getClient_id() == id)
            {
                System.out.println("Лот №"+number_product+" "+product.getName_product());
                number_product++;
                for (Integer bet : product.getBet())
                {
                    System.out.println("Ставка №"+number+" "+bet);
                    bet++;
                }
            }
        }
    }

    @Override
    public List<Client> displayClient() {
        return clientList;
    }


    public int getCounterLots(Client client)
    {
        int count =0;
        for(Product product: productList)
        {
            if(client.getId()==product.getClient_id())
            {
                count++;
            }
        }
        return count;
    }

    public int getCounterBets(Client client)
    {
        int count = 0;
        for(Product product: productList)
        {
            if(client.getId()==product.getClient_id())
            {
                count += product.getBet().size();
            }
        }
        return count;
    }
    public void fillClientsInfo() throws ParseException {
        String pattern = "yyyy-MM-dd";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
        Date d1 = simpleDateFormat.parse("2018-09-09");
        Date d2 = simpleDateFormat.parse("1997-03-02");
        Date d3 = simpleDateFormat.parse("1918-04-11");
        Date d4 = simpleDateFormat.parse("1999-09-09");
        Date d5 = simpleDateFormat.parse("1987-11-09");
        Date d6 = simpleDateFormat.parse("2000-11-06");
        Date d7 = simpleDateFormat.parse("2010-05-03");

        clientList.add(new Client(1,"Pavlov","Pavel","Pavlovich",d1));
        clientList.add(new Client(2,"Alenovna","Alena","Alenovna",d2));
        clientList.add(new Client(3,"Olegov","Oleg","Olegovich",d3));
        clientList.add(new Client(4,"Karlov","Karl","Bridge",d4));
        clientList.add(new Client(5,"Mbape","Killiani","Aphonasiev",d5));
        clientList.add(new Client(6,"Paki","Paul","Pafdf",d6));
        clientList.add(new Client(7,"Pauli","Paulinio","Paulijnjf",d7));
    }

    public  void showInfo() {

        for(Client client: clientList)
        {
                System.out.println("Name: "+client.getName()+"\nCount lots: "+getCounterLots(client)+"\nCount bets: "+
                        getCounterBets(client)+"\nDate : " +common.convertToNewFromat(client.getDate()));
        }
    }

    public void printProducts()
    {
        int number = 0;
        System.out.println();
        for(Product product: productList)
        {
            number++;
            System.out.println(number+") category = "+product.getCategory()+"; name = "+product.getName_product()
                    +"; price = "+product.getBet());
        }
    }

    public void sortByFio()
    {
        Comparator<Client> clientComp = new ClientCompLastName().thenComparing(new ClientCompName())
                .thenComparing(new ClientCompOtchestvo());
        Collections.sort(clientList, clientComp);
        for (Client c: clientList) {
            System.out.println(c.getLastname()+" "+c.getName()+" "+c.getOtchestvo());
        }
    }

    public void sortByDate()  {

        Collections.sort(clientList,new ClientCompDate());
        for(Client c: clientList){
            System.out.println(c.getName()+" "+common.convertToNewFromat(c.getDate()));
        }
    }

    public void fillTestData()
    {

        productList.add(new Product("Телефоны","iphone4s", new ArrayList<Integer>(Arrays.asList(1024,2456,3789)),1));
        productList.add(new Product("Телефоны","iphone5s", new ArrayList<Integer>(Arrays.asList(1222,2256,2389)),2));
        productList.add(new Product("Планшеты","sumsungA6", new ArrayList<Integer>(Arrays.asList(1344,2786,3789)),3));
        productList.add(new Product("Часы","casio", new ArrayList<Integer>(Arrays.asList(10278,24456,31789)),4));
        productList.add(new Product("Часы","fossil", new ArrayList<Integer>(Arrays.asList(10242,24565,33389)),5));
        productList.add(new Product("Телефоны","xiaomi A6", new ArrayList<Integer>(Arrays.asList(1044,2666,15789)),6));
        productList.add(new Product("Телефоны","xiaomi redmi A6", new ArrayList<Integer>(Arrays.asList(14,256,389)),7));
    }
}
