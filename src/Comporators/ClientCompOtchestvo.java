package Comporators;

import Models.Client;

import java.util.Comparator;

public class ClientCompOtchestvo implements Comparator<Client> {
    @Override
    public int compare(Client o1, Client o2) {
        return o1.getOtchestvo().compareTo(o2.getOtchestvo());
    }
}
