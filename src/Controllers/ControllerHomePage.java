package Controllers;

import Common.Common;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.stage.Stage;
import javafx.scene.control.Button;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class ControllerHomePage implements Initializable {
    @FXML
    private Button insert_btn;

    @FXML
    private Button view_btn;

    @FXML
    private Label label_id;

    private ResourceBundle resourceBundle;


    public void viewBtn(ActionEvent actionEvent) throws IOException {
        Common common = new Common();
        common.changeScene(actionEvent,"../Xml/View.fxml");
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        this.resourceBundle = resources;
        label_id.setText(ClientManager.getNameClient());
        if ((ClientManager.getNameClient().isEmpty() ||ClientManager.getLastNameClient().isEmpty()
        || ClientManager.getOtchestvo().isEmpty())){
            insert_btn.setDisable(true);
        }

    }

    public void insertBtn(ActionEvent actionEvent) throws IOException {
        Common common = new Common();
        common.changeScene(actionEvent,"../Xml/insertProduct.fxml");
    }
}
