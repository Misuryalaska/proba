package Models;

import java.util.ArrayList;

public class Product {
    private Long id;
    private String category;
    private String name_product;
    private ArrayList<Integer> bet;
    private int client_id;

    public Product(Long id, String category, String name_product, ArrayList<Integer> bet, int client_id) {
        this.id = id;
        this.category = category;
        this.name_product = name_product;
        this.bet = bet;
        this.client_id = client_id;
    }

    public Product(String category, String name_product, ArrayList<Integer> bet,int client_id) {
        this.category = category;
        this.name_product = name_product;
        this.bet = bet;
        this.client_id = client_id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getName_product() {
        return name_product;
    }

    public void setName_product(String name_product) {
        this.name_product = name_product;
    }

    public ArrayList<Integer>   getBet() {
        return bet;
    }

    public void setBet(ArrayList<Integer> bet) {
        this.bet = bet;
    }

    public int getClient_id() {
        return client_id;
    }

    public void setClient_id(int client_id) {
        this.client_id = client_id;
    }


    @Override
    public String toString() {
        return "Category: "+getCategory()+" Name: "+getName_product()+" Bet: "+getBet()+" Client_id: "+getClient_id();
    }
}
