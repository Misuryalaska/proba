package SecondTask;

import java.io.*;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

public class SecondTask {
    private static final String PATH = "C:\\Users\\Pavel\\Desktop\\content";
    public static void main(String[] args) throws IOException {
        File file = new File(PATH);
        log(file);
    }

    public static void log(File file) throws IOException {
        if (file.isDirectory()) {
            for(File item : file.listFiles())
            {
                if (item.isDirectory()) {
                    System.out.println(item.getName() + "папка, а не файл");
                }
                else {
                    //System.out.println(item.getName() + " \t file");
                    if(item.getName().contains("jpg") || item.getName().contains("jpeg")
                            || item.getName().contains("png")){
                        System.out.println("Контент не может быть обработан!");
                    }
                    else {
                        System.out.println(item.getName() + " \t file");
                        String name_file = item.getName();
                        List<String> lines = Files.readAllLines(Paths.get(PATH + "\\" + name_file), Charset.defaultCharset());
                        for (String words: lines)
                        {
                            System.out.println(words);
                        }
                    }
                }
            }

        }
    }
}
