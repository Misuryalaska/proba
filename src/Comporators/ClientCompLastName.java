package Comporators;

import Models.Client;

import java.util.Comparator;

public class ClientCompLastName implements Comparator<Client> {

    @Override
    public int compare(Client o1, Client o2) {
        return o1.getLastname().compareTo(o2.getLastname());
    }
}
