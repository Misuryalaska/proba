package Controllers;

import Common.Common;
import Controllers.ClientManager;
import Models.Client;
import Models.Product;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;

import java.io.IOException;
import java.net.URL;
import java.text.ParseException;
import java.util.Observable;
import java.util.ResourceBundle;

public class ControllerView extends Observable implements Initializable {

    @FXML
    private Button btnListCategory;

    @FXML
    private Button btnListBets;

    @FXML
    private Button btnYourLots;

    @FXML
    private Button btnBack;

    @FXML
    private Button btnInfoClients;

    @FXML
    private Button btnSortFio;

    @FXML
    private Button btnSortDate;

    @FXML
    private TableView tableLots;

    @FXML
    private TableColumn<Product, String> columnCategory;

    @FXML
    private TableColumn<Product, String> columnLots;

    @FXML
    private Label labelCount;

    private ResourceBundle resourceBundle;
    private ClientManager clientManager = new ClientManager();


    public void actionButtonPressed(javafx.event.ActionEvent actionEvent) throws IOException {

        Object source = actionEvent.getSource();

        if(!(source instanceof Button)){
            return;
        }

        Button clickedButton = (Button) source;

        Product selectedProduct = (Product) tableLots.getSelectionModel().getSelectedItem();


        switch (clickedButton.getId())
        {
            case "btnListCategory":
                String category = selectedProduct.getCategory();
                System.out.println(category+":");
                ClientManager.getLotsWithCategory(category);
                break;
            case "btnListBets":
                String lot = selectedProduct.getName_product();
                System.out.println(lot+":");
                ClientManager.getBetsWithLots(lot);
                break;
            case "btnYourLots":
                ClientManager.getYourLotsAndBets(Common.newId());
                break;
            case "btnInfoClients":
                clientManager.showInfo();
                break;
            case "btnSortFio":
                clientManager.sortByFio();
                break;
            case "btnSortDate":
                clientManager.sortByDate();
                break;
        }
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        this.resourceBundle = resources;
        columnCategory.setCellValueFactory(new PropertyValueFactory<Product, String>("category"));
        columnLots.setCellValueFactory(new PropertyValueFactory<Product, String>("name_product"));
        if (!(ClientManager.getNameClient().equals("admin") && ClientManager.getLastNameClient().equals("admin")
                && ClientManager.getOtchestvo().equals("admin"))){
            btnInfoClients.setDisable(true);
            btnSortDate.setDisable(true);
            btnSortFio.setDisable(true);
        }
        fillData();
        try {
            clientManager.fillClientsInfo();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        tableLots.setItems(clientManager.displayProducts());
    }

    private void fillData() {
        clientManager.fillTestData();
    }


    public void btnBack(ActionEvent actionEvent) throws IOException {
        Common common = new Common();
        common.changeScene(actionEvent,"../Xml/homepage.fxml");
    }
}
