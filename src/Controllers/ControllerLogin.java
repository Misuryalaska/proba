package Controllers;

import Common.Common;
import Controllers.ClientManager;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

import java.io.IOException;

public class ControllerLogin {

    @FXML
    private TextField ed_name;

    @FXML
    private TextField ed_lastname;

    @FXML
    private TextField ed_otchestvo;

    @FXML
    private DatePicker dating;

    public void login(ActionEvent actionEvent) throws IOException {
        Common common = new Common();

        ClientManager.writeUser(Common.newId(),ed_lastname.getText(),ed_name.getText(),ed_otchestvo.getText(),common.convertToDate(dating.getValue()));


        common.changeScene(actionEvent,"../Xml/homepage.fxml");

    }


}
