package Controllers;

import Common.Common;
import Controllers.ClientManager;
import Models.Product;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.MenuButton;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

import java.io.IOException;
import java.util.ArrayList;

public class ControllerInsertProduct {

    @FXML
    private MenuButton category_menu_btn;

    @FXML
    private TextField name;

    @FXML
    private TextField price;

    public void doneButtonAction(ActionEvent actionEvent) throws IOException {
        String category = category_menu_btn.getText();
        String name_product = name.getText();
        ArrayList<Integer> bets = new ArrayList<>();
        bets.add(Integer.parseInt(price.getText()));
        Product p1 = new Product(category,name_product,bets, Common.newId());
        ClientManager.add(p1);
        Common common = new Common();
        common.changeScene(actionEvent,"../Xml/View.fxml");

    }

    public void categoryMenuItemButtonAction(ActionEvent actionEvent) {
        MenuItem menu = (MenuItem) actionEvent.getSource();
        category_menu_btn.setText(menu.getText());
    }
}
